angular
  .module('app')
  .config(routesConfig);

/** @ngInject */
function routesConfig($stateProvider, $urlRouterProvider, $locationProvider) {
  $locationProvider.html5Mode(true).hashPrefix('!');
  $urlRouterProvider.otherwise('/');

  var objectStateIndex = {
  	name: 'objectStateIndex',
  	url: '/',
  	templateUrl: 'app/object-states/object-states.index.html',
  	controller: 'ObjectStateController',
  	controllerAs: 'vm'
  };

  $stateProvider.state(objectStateIndex);
}
