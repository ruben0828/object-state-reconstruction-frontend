(function() {
	'use strict';

	angular.module('app')
		.controller('ObjectStateController', ObjectStateController);

	/** @ngInject */
	function ObjectStateController($log, $uibModal, ObjectStateService) {
		var vm = this;
		vm.alert = {};
		vm.closeAlert = closeAlert;
		vm.form = {};
		vm.result = null;
		vm.showImportCSVDlg = showImportCSVDlg;
		vm.submit = submit;

		function closeAlert() {
			vm.alert.show = false;
		}

		function showImportCSVDlg() {
			var dialog = {
				animation: true,
				ariaLabelledBy: 'modal-title',
				ariaDescribedBy: 'modal-body',
				templateUrl: 'app/object-states/import.html',
				controller: 'ImportCSVController',
				controllerAs: 'vm'
			};
			var modalInstance = $uibModal.open(dialog);

			modalInstance.result.then(function(data) {
				$log.log('modal close: ', data);
				vm.alert = {
					type: 'success',
					timeout: 2000,
					msg: 'CSV data has been uploaded successfully.',
					show: true
				}
			}, function() {
				$log.log('modal dismissed');
			});
		}

		function submit() {
			$log.log("form: ", vm.form);
			return ObjectStateService.getStates(
					vm.form.id,
					vm.form.type,
					vm.form.timestamp)
				.then(function(data) {
					$log.log("Submit response: ", data);
					vm.result = data;
				}).catch(function(error) {
					$log.error('Error: ', error);
				});
		}
	}
})();