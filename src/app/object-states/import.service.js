(function() {
	'use strict';

	angular.module('app')
		.factory('ImportCSVService', ImportCSVService);

	/** @ngInject */
	function ImportCSVService($http, $log, Upload) {
		// const API_URL = 'http://localhost:3000/api/v1';
		const API_URL = 'http://object-states-reconstruction.herokuapp.com/api/v1';
		var service = {
			importCSV: importCSV
		};
		return service;

		function importCSV(file) {
			return Upload.upload({
					url: API_URL + '/my_objects/import',
					data: {file: file}
				})
				.then(function(response) {
					return response.data;
				}).catch(function(error) {
					$log.error('XHR failed for importCSV: ' + error.data);
				})
		}
	}
})();