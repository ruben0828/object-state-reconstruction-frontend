(function() {
	'use strict';

	angular.module('app')
		.controller('ImportCSVController', ImportCSVController);

	/** @ngInject */
	function ImportCSVController($log, $uibModalInstance, ImportCSVService) {
		var vm = this;
		vm.csv_file = null;
		vm.cancel = cancel;
		vm.submit = submit;

		function cancel() {
			$uibModalInstance.dismiss('cancel');
		}

		function close() {
			$uibModalInstance.close();
		}

		function submit() {
			return ImportCSVService.importCSV(vm.csv_file)
				.then(function(data) {
					$log.log('Import successfull!');
					close();
				}).catch(function(error) {
					$log.error("Import error: ", error);
				});
		}
	}
})();