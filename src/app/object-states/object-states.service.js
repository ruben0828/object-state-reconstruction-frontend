(function() {
	'use strict';

	angular.module('app')
		.factory('ObjectStateService', ObjectStateService);

	/** @ngInject */
	function ObjectStateService($http, $log) {
		// const API_URL = 'http://localhost:3000/api/v1';
		const API_URL = 'http://object-states-reconstruction.herokuapp.com/api/v1';
		var service = {
			getStates: getStates
		};
		return service;

		function getStates(id, type, timestamp) {
			return $http.get(API_URL + '/my_objects/' + id + '/type/' + type + '/timestamp/' + timestamp)
				.then(function(response) {
					return response.data;
				}).catch(function(error) {
					$log.error('XHR failed for getStates: ' + error.data);
				})
		}
	}
})();